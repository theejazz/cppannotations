//#define XERR
#include "inilist.ih"

IniList::~IniList()
{
    if (d_id == d_owner)
    {
        s_memory[d_index] = false;
        cout << "destroying object " << d_id << " and memory[" << 
                d_index << "]\n";
    }
    else
    {
        cout << "destroying object " << d_id << ".\n";
        check("");
    }
}
