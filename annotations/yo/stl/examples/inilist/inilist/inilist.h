#ifndef INCLUDED_INILIST_
#define INCLUDED_INILIST_

#include <vector>

class IniList
{
    static size_t s_count;              // counts the # of objects: used as
                                        // object ID
    static std::vector<bool> s_memory;  // values 'true' indicate available
                                        // memory. Each constructed obj. has
                                        // its own index; copied objects use
                                        // their 'other' index value

    size_t d_id;            // using s_count
    size_t d_owner;         // owner's ID: the owner deletes the memory
    size_t d_index;         // index into s_memory

    public:
        IniList();
        IniList(IniList const &other);
        ~IniList();

        void check(char const *label);  // checks the availability of its
                                        // memory
};
        
#endif
