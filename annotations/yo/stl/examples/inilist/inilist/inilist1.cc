//#define XERR
#include "inilist.ih"

IniList::IniList()
:
    d_id(s_count++),            // set the object ID
    d_owner(d_id),              // this is the owner of the memory
    d_index(s_memory.size())    // and the index of its memory element
{
    s_memory.push_back(true);

    cout << "created object " << d_id << " using memory[" << d_index << "]\n";
}


