//#define XERR
#include "main.ih"

vector<bool> IniList::s_memory;

IniList factory(IniList const &src)
{
    cout << "factor(IniList const &)\n";
    return src;
}

int main(int argc, char **argv)
{
    cout << "\n"
            "starting main, after constructing 'map'\n"
            "\n";

    IniList i1;

    {
        IniList i2(i1);
        i2.check("copy: ");
    }

    i1.check("check: ");

    cout << '\n';

    IniList i0(factory(IniList{}));     // i0 has lost access to its memory!
    i0.check("via factory: ");

    cout << "\n"
            "Now the map:\n"
            "\n";

    map.find(1)->second.check("element from map: ");
}   






