Simple:
=======
template <typename Type> 
concept BasicMath =
    requires(Type lhs, Type rhs)
    {
        lhs + rhs;      // addition must be available
    };

also calling a function may be used as simple requirement:

template< class T >
concept swappable =
    requires(T &a, T &b) 
    {
        ranges::swap(a, b);
    };

Type:
=====
template <typename Type> 
concept HasValueType =
    requires()
    {
        typename Type::value_type;
    };

Compound:
=========
concept ReturnType =
    requires(Type par)
    {
        { par[0] } -> std::same_as<Ret>;     // par[..] returns a Ret
    }
 
Nested:
=======
template <typename Type>
concept BiIterator =
    FwdIterator<Type>
    and
    requires(Type type)
    {
        --type;
    };

templates using concepts:
=========================
template <BiIterator Type>
void biFun(Type tp)
{}
---------------------------
template <typename Type>
void biFun(Type tp)
    requires BiIterator<Type>
{}

